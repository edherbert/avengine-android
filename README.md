# avEngine-android

#### A wrapper project to build the avEngine on android
This is an android project which wraps around the avEngine so it can be built for android.

Ensure you clone with submodules to properly setup the repository.
```bash
git clone --recurse-submodules --shallow-submodules https://gitlab.com/edherbert/avEngine-android.git
```